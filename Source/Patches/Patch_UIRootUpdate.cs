﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Restarter
{
    [HarmonyPatch(typeof(UIRoot), "UIRootUpdate")]
    public static class Patch_UIRootUpdate
    {
        [HarmonyPostfix]
        public static void DetectRestartButtonPress()
        {
            try
            {
                if(Restarter_Defs.Restarter_Restart.JustPressed)
                {
                    RestartUtility.DoRestart();
                }
            }
            catch(Exception e)
            {
                string errorMessage = $"Could not intercept restart keypress, exception: {e}";
                Log.ErrorOnce(errorMessage, errorMessage.GetHashCode());
            }
        }
    }
}
