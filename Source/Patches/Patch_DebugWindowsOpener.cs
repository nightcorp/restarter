﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace Restarter.Patches
{
    [HarmonyPatch(typeof(DebugWindowsOpener), "DrawButtons")]
    public static class Patch_DebugWindowsOpener
    {
        [HarmonyPostfix]
#if v1_3 || v1_4
        public static void AddRestartButton(WidgetRow ___widgetRow)
#else
        public static void AddRestartButton(WidgetRow ___widgetRow, ref float ___widgetRowFinalX)
#endif
        {
            try
            {
                if(___widgetRow.ButtonIcon(TexButton.Reload, "Restart"))
                {
                    RestartUtility.DoRestart();
                }
#if !v1_3 && !v1_4
                ___widgetRowFinalX = ___widgetRow.FinalX;
#endif
            }
            catch(Exception e)
            {
                string errorMessage = $"Could not add restart button, exception: {e}";
                Log.ErrorOnce(errorMessage, errorMessage.GetHashCode());
            }
        }
    }
}
