﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;

namespace Restarter
{
    public static class RestartUtility
    {
        /// <summary>
        /// Locking variable, prevents calling the restart multiple times (which leads to opening multiple RimWorld instances)
        /// </summary>
        static bool isRestarting = false;
        public static void DoRestart()
        {
            if (isRestarting)
            {
                return;
            }
            GenCommandLine.Restart();
        }
    }
}
