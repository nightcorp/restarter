﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Restarter
{
    public static class Restarter_Defs
    {
        public static KeyBindingDef Restarter_Restart = DefDatabase<KeyBindingDef>.GetNamed("Restarter_Restart");
    }
}
